
const FIRST_NAME = "Ana-Maria";
const LAST_NAME = "Schpak";
const GRUPA = "1090";

/**
 * Make the implementation here
 */
class Employee {
    constructor(name, surname, salary){
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }

    getDetails() { 
        return this.name + " " + this.surname + " " + this.salary;       
    }

}

class SoftwareEngineer extends Employee {
   constructor(name, surname, salary, experience){
       super(name, surname, salary);
       if(!experience){
            this.experience = 'JUNIOR';
       }
       else{
       this.experience = experience;
       }    
   }

   applyBonus(){
       if(this.experience === 'JUNIOR'){
           return this.salary * 1.1;
       }
       if(this.experience === 'MIDDLE'){
        return this.salary * 1.15;
       }
       if(this.experience === 'SENIOR'){
        return this.salary * 1.2;
       }
       if(this.experience !== 'JUNIOR' && this.experience !== 'MIDDLE' && this.experience !== 'SENIOR'){
           return this.salary * 1.1;
       }

   }
}


module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

